
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AC 2017 - Daniel Camba Lamas %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%#ok<*NASGU>

main();

function main()

    global M

    % --- 1.2.1 --
        % Ver la función "bezier4". El parametro SAMPLES aumenta la
        % precisión en el calculo de la longitud de la curva, aunque
        % llegado un punto el incremento del valor no presenta mejora
        % en un orden de 5 o 6 decimales.
    % --- End 1.2.1 ---

    % --- 1.2.2 ---
    M1 = [   5     -10
            10      40
            37      -5
            45      15  ];
    M = M1;
    samples = 1000;
    [a, s] = bezier4( m(1)  ,m(2)  ,m(3)  ,m(4)  ,samples);

    % Equidistant in T
    fig(-15, 60, '1.2.2 - Equidistantes en T');
    plotBezier4(a);
    for i = 1:100:2*length(a)-1
        scatter(a(i), a(i+1), 'o', 'red');
    end

    % Equidistant in S
    fig(-15, 60, '1.2.2 - Equidistantes en S');
    plotBezier4(a);
    dist = 0;
    slice = 5; % Modify to space points in X percentage
    for i = 3:2:2*length(a)-2
        dist = dist + ( distance( a(i-2),a(i-1) , a(i),a(i+1) ) );
        if abs(dist - s * (slice/100) ) <= 0.3 % 0.3 is the error tolerance
            dist = 0;
            scatter( a(i), a(i+1), 'o', 'blue')
        end
    end
    % --- End 1.2.2 ---

    % --- 1.2.3 ---
        M2 = [   5     -10;
                13      -2;
                 5      20;
                25      20;
                45      20;
                35      -2;
                43     -10;
                50     -17;
                20     -17;
                12.5   -10;
                 5.5     0;
                 0     -15  ];
        M = M2;
        samples = 1000;
        fig(0, 50, '1.2.3 - Concantenar y transformar Beziers');

        a = bezier4( m(1)  ,m(2)  ,m(3)  ,m(4)  ,samples);
        plotBezier4(a);
        plotBezier4TRS(a);

        b = bezier4( m(4)  ,m(5)  ,m(6)  ,m(7)  ,samples);
        plotBezier4(b);
        plotBezier4TRS(b);

        c = bezier4( m(1)  ,m(12) ,m(11) ,m(10) ,samples);
        plotBezier4(c);
        plotBezier4TRS(c);

        d = bezier4( m(10) ,m(9)  ,m(8)  ,m(7)  ,samples);
        plotBezier4(d);
        plotBezier4TRS(d);
    % --- End 1.2.3 ---
end

% compute a bezier curve given 4 points - [ Exercise: 1.2.1 ]
function [ y, s ] = bezier4(p1,p2,p3,p4,samples)
    t = linspace(0,1,samples);
    y = kron((1-t).^3,p1) + kron(3*(1-t).^2.*t,p2) + kron(3*(1-t).*t.^2,p3) + kron(t.^3,p4);
    s = 0;
    for i = 1:2:(length(y)*2)-2
        s = s + ( distance( y(i),y(i+1), y(i+2),y(i+3) ) );
    end
end

% plot a bezier curve
function plotBezier4(pts)
    plot(pts(1,:),pts(2,:));
end


% --- HELPERS ---

function fig(x,y,str)
    figure;
    title(str)
    hold on;
    xlim([x y]);
    axis equal;
    plotPoints();
end

% a simple way to catch a point from a (global) matrix
function y = m(row)
    global M
    y = [M(row,1); M(row,2)];
end

% plot points from a (global) matrix
function plotPoints()
    global M
    for i = 1:1:length(M)
        scatter(M(i,1), M(i,2),75,'s','black','filled');
    end
end

% plot a modified bezier curve - [ For exercise: 1.2.3 ]
function plotBezier4TRS(pts)
    T = 50; % translate
    S = [(1/3), 0.5]; % scale
    R = -S(2); % "rotate"
    plot( ( T + pts(1,:) ) * S(1) , pts(2,:) * R );
end
